﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    float horizontal;
    public float speed;
    Rigidbody2D rbc;
    public float jump;
    public Flowchart fc;
    static public bool hasWarehouseKey = false;
    public GameObject CoverBlock;
   

    bool grounded = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    void Awake()
    {
        rbc = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        if (fc.GetBooleanVariable("HasWarehouseKey") == true)
            hasWarehouseKey = true;

        fc.SetBooleanVariable("HasWarehouseKey", hasWarehouseKey);
       
       if (fc.GetBooleanVariable("Talking") == false)
        {
            if (grounded && Input.GetAxis("Jump") > 0)
            {

                rbc.velocity = new Vector2(rbc.velocity.x, 0f);
                rbc.AddForce(new Vector2(0, jump), ForceMode2D.Impulse);
                grounded = false;
            }

            horizontal = Input.GetAxisRaw("Horizontal");
        }
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
      
    }

    void FixedUpdate()
    {
       if (!fc.GetBooleanVariable("Talking"))
        {
            rbc.velocity = new Vector2(horizontal * speed, rbc.velocity.y);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Climbable")
        {
            Debug.Log("Climbable");
        }

      

        

        if (other.tag == "HouseLayer") {
            CoverBlock.GetComponent<SpriteRenderer>().enabled = false;
        }
        if (other.tag == "OutsideHouse")
        {
            CoverBlock.GetComponent<SpriteRenderer>().enabled = true;
        }

  

        if (other.name == "TriggerForCutscene") {
            fc.SendFungusMessage("TriggerJeremy");
            Destroy(other.gameObject);
            SendMessage("MessageRecieved");
        }

        if (other.name == "Suicide Building") {
            fc.SendFungusMessage("SuicideBuilding");
        }
        if (other.name == "Suicide Ground") {
            if (fc.GetBooleanVariable("SeenRoof"))
            {
                fc.SendFungusMessage("SuicideFloor");
                other.GetComponent<BoxCollider2D>().enabled = false;
            }
        }

        if(other.name == "ExitScene"){
            SceneManager.LoadScene("Warehouse_Exterior_1"); 
        }
    }

    
    public void ChangeLayer(int layer) {
        this.GetComponent<SpriteRenderer>().sortingOrder = layer;
    }
}
