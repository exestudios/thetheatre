﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.SceneManagement;

public class ApartmentManager : MonoBehaviour
{
    public GameObject player;
    PlayerController pc;
    public Flowchart fc;


    public int nextScene;

    public GameObject[] apartmentItems = new GameObject[5];

    public GameObject exitDoor;

    string objectName;

    private void Awake()
    {
        pc = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fc.GetBooleanVariable("FoundKey"))
        {
            exitDoor.GetComponent<BoxCollider2D>().enabled = true;
        }

    }

    public void Interacted(GameObject gm)
    {
        objectName = gm.name;

        if (objectName == "Door")
        {
            gm.GetComponent<Door>().Open();

        }

        else if (objectName == "Phone")
        {
            fc.SendFungusMessage("Phone");
            gm.GetComponent<BoxCollider2D>().enabled = false;
            foreach (GameObject gO in apartmentItems)
            {
                gO.GetComponent<BoxCollider2D>().enabled = true;
            }
        }

        else if (objectName == "Bed")
        {
            fc.SendFungusMessage("Bed");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (objectName == "Mirror")
        {
            fc.SendFungusMessage("Mirror");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (objectName == "Plant")
        {
            fc.SendFungusMessage("Plant");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (objectName == "Trash")
        {
            fc.SendFungusMessage("Trash");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (objectName == "TV")
        {
            fc.SendFungusMessage("TV");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (objectName == "BackDoor")
        {
      
            fc.SendFungusMessage("BackDoor");
            Debug.Log("hello?!?");
            
                

        }

    }
    void ChangeScene() {
        Debug.Log("hello?");
        SceneManager.LoadScene("House Level");
    }
}
  