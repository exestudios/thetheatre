﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class AudioController : MonoBehaviour
{
    public Flowchart fc;
    AudioSource aS;
    // Start is called before the first frame update
    void Awake()
    {
        aS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fc.GetBooleanVariable("LostGame") == true) {
            aS.volume = 0;
        }
        if (fc.GetBooleanVariable("LostGame") == false)
        {
            if (fc.GetBooleanVariable("Talking") == true)
            {
                aS.volume = 0.25f;
            }
            else
            {
                aS.volume = 0.75f;
            }
        }
    }
}
