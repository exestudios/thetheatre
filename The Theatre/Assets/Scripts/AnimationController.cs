﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class AnimationController : MonoBehaviour
{
    private Animator animator;
    public bool facingRight;
    public Rigidbody2D rb2D;

    public SpriteRenderer signalDisplay;

    public Flowchart fc;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        animator.SetBool("HasLadder", fc.GetBooleanVariable("CarryingLadder"));
        animator.SetFloat("Movement", rb2D.velocity.x);

        if (fc.GetBooleanVariable("CarryingLadder")){
            animator.SetBool("HasLadder", true);
        }

        //if giving walk input
        if (Input.GetButton("Horizontal"))
        {
            //if has ladder
            if (animator.GetBool("HasLadder")){
                //walk with ladder
                animator.SetBool("LadderWalk", true);
            }

            //no ladder walk
            animator.SetBool("Walk", true);

            //if moving right
            if (animator.GetFloat("Movement") > 0.5)
            {
                //display right
                transform.localScale = new Vector3(-1f, 1f, 1f);
                //set facing right to true
                facingRight = true;
            }
            //if moving left
            else if (animator.GetFloat("Movement") < 0.5)
            {
                //display left
                transform.localScale = new Vector3(1f, 1f, 1f);
                facingRight = false;
            }
        }
        //else if no input is given
        else
        {
            //if they have a ladder
            if (animator.GetBool("HasLadder"))
            {
                //Ladder idle
                animator.SetBool("LadderWalk", false);
            }
            //walk idle
            animator.SetBool("Walk", false);


        }

        //Signal Flipper
        if (facingRight == true)
        {
            signalDisplay.flipX = true;
        } else if (facingRight == false)
        {
            signalDisplay.flipX = false;
        }
        

        /*
        //if has ladder 
        if (fc.GetBooleanVariable("CarryingLadder"))
        {
            //display ladder walk
            animator.SetBool("hasLadder", true);
        }
        */
        
    }
    
}
