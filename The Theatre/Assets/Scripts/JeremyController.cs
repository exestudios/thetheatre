﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class JeremyController : MonoBehaviour
{
    public Flowchart fc;
    AudioSource aS;
    public AudioClip shoot;
    public AudioClip drop;
    int count = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (fc.GetBooleanVariable("JeremyDead") == true && count == 0 ) {
            StartCoroutine(KillJeremy());
            count++;
        }
    }

    IEnumerator KillJeremy() {
        yield return new WaitForSeconds(1);
        aS = GetComponent<AudioSource>();
        aS.clip = shoot;
        aS.Play();
        Debug.Log("Jeremy is dead");
        StartCoroutine(DropShell());
    }

    IEnumerator DropShell() {
        yield return new WaitForSeconds(2f);
        aS.clip = drop;
        aS.Play();
    }
}
