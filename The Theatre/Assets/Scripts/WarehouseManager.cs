﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.SceneManagement;

public class WarehouseManager : MonoBehaviour
{
    public GameObject player;
    PlayerController pc;
    public Flowchart fc;

    public GameObject fenceClosed;
    public GameObject fenceOpen;


    static bool hasKey;
    public GameObject outsideColliders;
    public GameObject insideColliders;

    public GameObject outsideImage;
    public GameObject triggerForCutscene;

    public GameObject pliers;

    public int nextScene;

    string objectName;

    private void Awake()
    {
        if (fc.GetBooleanVariable("FenceOpen") == true) {
            OpenFence();
        }
        pc = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fc.GetBooleanVariable("FoundKey"))
        {
            hasKey = true;
        }

    }

    public void Interacted(GameObject gm)
    {
        objectName = gm.name;

        if (objectName == "Door")
        {
            gm.GetComponent<Door>().Open();
        }

        if (objectName == "Shipping Crate") {
            gm.GetComponent<Door>().Open();
        }

        if (gm == outsideImage) {
            outsideImage.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (objectName == "DoorFence")
        {
            fc.SendFungusMessage("FenceDoor");
            if (PlayerController.hasWarehouseKey == true)
            {
                gm.GetComponent<Door>().Open();
            }
        }

        if (objectName == "WorkBench")
        {
            fc.SendFungusMessage("WorkBench");
        }

        if (objectName == "Fence Hole") {
            fc.SendFungusMessage("Fence Hole");
            
        }

        if (objectName == "Key Brush")
        {

            fc.SendFungusMessage("Key Brush");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        if (objectName == "Fence")
        {
            fc.SendFungusMessage("Fence");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }
        if (objectName == "Pliers")
        {
            fc.SendFungusMessage("Pliers");
            gm.GetComponent<BoxCollider2D>().enabled = false;
            pliers.SetActive(false);
        }
        if (objectName == "Trapdoor")
        {
            fc.SendFungusMessage("Trapdoor");

        }
        if (objectName == "Suicide Building")
        {
            fc.SendFungusMessage("SuicideBuidling");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }

       
      
    }

    public void MessageRecieved()
    {
        Interacted(outsideImage);
    }

    public void ChangeScenesExt() {
        SceneManager.LoadScene("Warehouse_Interior");
    }

    public void ChaingeScenesInt() {
        SceneManager.LoadScene("Warehouse_Ext_1");
    }

    void OutsideToggle() {
        if (outsideColliders.activeSelf == true)
        {
            outsideColliders.SetActive(false);
            insideColliders.SetActive(true);
            pc.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
        else
        {
            outsideColliders.SetActive(true);
            insideColliders.SetActive(false);
            pc.GetComponent<SpriteRenderer>().sortingOrder = 5;
        }
    }
    void OpenFence() {
        fenceClosed.SetActive(false);
        fenceOpen.SetActive(true);
    }
}
