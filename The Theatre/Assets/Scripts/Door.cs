﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Door : MonoBehaviour
{
    SpriteRenderer spriteRenderer;

    public Sprite openDoor;
    public Sprite closedDoor;

    public AudioClip open;

    AudioSource aS;

    public GameObject myColliderChild;


    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    //Recieve Message
    public void Open()
    {
        spriteRenderer.sprite = openDoor;
        this.gameObject.AddComponent<AudioSource>();
        aS = GetComponent<AudioSource>();
        aS.clip = open;
        aS.Play();
        this.GetComponent<BoxCollider2D>().enabled = false; 
        //delete the block
        myColliderChild.SetActive(false);

        if (this.gameObject.name == "DoorLocked") {
            this.GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}
