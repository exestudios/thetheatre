﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.SceneManagement;

public class HouseManager : MonoBehaviour
{
    //Global Public References required for every level
    public GameObject player;
    public Flowchart fc;
    public AudioSource aS;
    public int nextScene;

    //Name of the object the manager references
    string objectName;
    public GameObject ladder;
    public GameObject ladderSpot;
    public GameObject ladderSprite;

    //Conditional variables only dependant on single scene


    //Interacted finds a GameObject and gets the name of the object, most of the time sending a message to Fungus.
    public void Interacted(GameObject gm)
    {
        objectName = gm.name;

        if (objectName == "Door")
        {
            gm.GetComponent<Door>().Open();
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }

        else if (objectName == "Dresser")
        {
            fc.SendFungusMessage("Dresser");
        }

        else if (objectName == "Background Door")
        {
            fc.SendFungusMessage("BackgroundDoor");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }

        else if (objectName == "Broken Stairs")
        {
            fc.SendFungusMessage("BrokenStairs");
            gm.GetComponent<BoxCollider2D>().enabled = false;
        }

        else if (objectName == "Backyard Door")
        {
            fc.SendFungusMessage("BackyardTrigger");
            if (fc.GetBooleanVariable("HasKey"))
            {
                gm.GetComponent<BoxCollider2D>().enabled = false; ;
            }
        }

        else if (objectName == "Ladder")
        {
            fc.SendFungusMessage("LadderUp");
            gm.GetComponent<BoxCollider2D>().enabled = false;
            ladderSpot.GetComponent<BoxCollider2D>().enabled = true;
            ladderSprite.SetActive(false);
            
        }

        else if (objectName == "LadderSpot")
        {
            gm.GetComponent<BoxCollider2D>().enabled = false;
            fc.SendFungusMessage("LadderDown");
            ladder.SetActive(true);
        }

        else if (objectName == "BadDoor")
        {
            gm.GetComponent<Door>().Open();
            aS.Stop();
            fc.SendFungusMessage("BadDoor");
            
        }

    }

    void ChangeScenes() {
        Debug.Log("hello?!");
        SceneManager.LoadScene("Warehouse_Exterior_1");
    }
 }
