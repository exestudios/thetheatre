﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseInteraction : MonoBehaviour
{
    public GameObject signalDisplayObj;
    public bool signalDisplay = false;


    public HouseManager hm;
    public GameObject houseDisplay;


    public GameObject objectImHitting;

    void Update()
    {
        if (signalDisplay == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                {
                    hm.Interacted(objectImHitting);
                }
            }
        }
    }

    //If you collide
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //With an interactable object 
        if (collision.gameObject.tag == "Interactable")
        {
            //Print this line

            //Set Signal Display to Active
            signalDisplay = true;
            //Display Signal Object
            signalDisplayObj.SetActive(true);
            //set objectImHitting
            objectImHitting = collision.gameObject;

        }
        if (collision.gameObject.tag == "HouseLayer") {
            Debug.Log("Remove Foreground");
            houseDisplay.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
    //If you un-collide
    private void OnTriggerExit2D(Collider2D collision)
    {
        //With an interactable object
        if (collision.gameObject.tag == "Interactable")
        {
            //Print this line

            //Set Signal Display to Active
            signalDisplay = false;
            //Stop Display Signal Object
            signalDisplayObj.SetActive(false);
        }
        if (collision.gameObject.tag == "HouseLayer") {
            houseDisplay.GetComponent<SpriteRenderer>().enabled = true;
            Debug.Log("Add Foreground");
    
        }
    }
}