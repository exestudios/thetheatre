﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TestScenePlayerController : MonoBehaviour
{
    bool inHazard = false;
    int health = 5;
    public float jump;
    float horizontal;
    bool grounded = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public int speed;

    Rigidbody2D rbc;


    void Awake()
    {
        rbc = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health > 0)
        {
            if (grounded && Input.GetAxis("Jump") > 0)
            {

                rbc.velocity = new Vector2(rbc.velocity.x, 0f);
                rbc.AddForce(new Vector2(0, jump), ForceMode2D.Impulse);
                grounded = false;
            }

            horizontal = Input.GetAxisRaw("Horizontal");

            grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
        }
        if (health == 0) {
            Destroy(this.gameObject);
        }
    }

    void FixedUpdate()
    {


        rbc.velocity = new Vector2(horizontal * speed, rbc.velocity.y);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Obstacle")
        {
            StartCoroutine(GetHit());
            inHazard = true;
        }

        if (other.tag == "Collectable") {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Obstacle")
        {
            inHazard = false;
        }
    }


    IEnumerator GetHit() {
        health--;
        yield return new WaitForSeconds(1.0f);
        if (inHazard == true) {
            StartCoroutine(GetHit());
        }
    }
}
